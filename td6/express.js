const express = require('express');
const cors = require('cors')
const app = express();
const morgan = require('morgan'); // Middleware de logging

app.get('/', function(req, res) {
  res.send('Hello !');
});

app.get('/home', function(req, res) {
  res.send('Maison :-)');
});

app.get('/user/:uid', function(req, res) {
  res.send('Bonjour utilisateur ' + req.params.uid);
});

app.use(function(req, res, next){
  res.status(404).send('Page introuvable !');
});

app.use(cors())
app.use(morgan('combined'));
app.listen(8080, function() {
  console.log('Example app listening on port 8080!');
});
