const DATA = {
    0: {
        title: "Toilettes",
        url: 'sigsanitaire'
    },
    1: {
        title: "Aire de jeux pour enfants",
        url: 'airejeux'
    },
    2: {
        title: "Quartiers",
        url: 'sigquartiers'
    },
}

loadData = (index) => {
    if(typeof DATA[index] === "undefined") {
        console.error("Index not found")
        return
    }
    clearData(DATA[index].title)
    const URL = 'http://odata.bordeaux.fr/v1/databordeaux/' + DATA[index].url + '/?format=json&callback=?';
    $.getJSON(URL, function(result){
        displayData(result)
    });
}

clearData = (title) => {
    document.getElementById('categorySelected').innerText = title;
    document.getElementById('data-list').innerHTML = "";
}

displayData = json => {
    const dataList = document.getElementById('data-list')
    console.log(json.d)
    json.d.forEach(element => {
        const container = document.createElement('div')

        const name = document.createElement('h3')
        name.innerText = element.nom
        container.appendChild(name)
        
        const address = document.createElement('p')
        address.innerText = "Adresse : " + element.adresse
        container.appendChild(address)

        const quartier = document.createElement('p')
        quartier.innerText = "Quartier : " + element.quartier
        container.appendChild(quartier)

        dataList.appendChild(container)
    });
    if(json.d.length <= 0) {
        const el = document.createElement('p')
        el.innerText = "No data."
        dataList.appendChild(el)
    }
}

document.getElementById('cat-toilets').addEventListener('click', function() {
    loadData(0);
});
document.getElementById('cat-kidareas').addEventListener('click', function() {
    loadData(1);
});
document.getElementById('cat-quartiers').addEventListener('click', function() {
    loadData(2);
});
