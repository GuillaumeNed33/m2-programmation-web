loadData = () => {
    const city = document.getElementById('input-city').value
    console.log(city)
    const URL = 'https://www.prevision-meteo.ch/services/json/' + city;
    fetch(URL, {
        method: 'GET',
        mode: 'cors',
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        clearData()
        displayData(data)
    })
    .catch(err => {
        clearData()
        console.error(err)
        document.getElementById('data').innerHTML = "<p class='error'>Une erreur est survenue.</p>"
    })
}

clearData = () => {
    document.getElementById('data').innerHTML = "";
}

displayData = json => {
    console.log(json)
    const title = document.createElement('h1');

    if(json.errors) {
        const t = document.createTextNode("Une erreur est survenue");
        title.appendChild(t) 
        
        const error = document.createElement('p');
        const tError = document.createTextNode(json.errors[0].text);
        error.appendChild(tError) 
    
        const description = document.createElement('p');
        const tDesc = document.createTextNode(json.errors[0].description);
        description.appendChild(tDesc) 

        document.getElementById('data').appendChild(title)
        document.getElementById('data').appendChild(error)
        document.getElementById('data').appendChild(description)
    } else {
        const t = document.createTextNode(json.city_info.name);
        title.appendChild(t)

        const description = document.createElement('p');
        const tDesc = document.createTextNode("Condition : " + json.current_condition.condition);
        description.appendChild(tDesc)

        document.getElementById('data').appendChild(title)
        document.getElementById('data').appendChild(description)
    }
}

let map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: --0.57918, lng: 144.837789},
    zoom: 8
  });
}

document.getElementById('display-meteo-btn').addEventListener('click', function() {
    loadData();
});