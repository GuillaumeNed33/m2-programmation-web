let money = 100
let bank_money = 100
let level, bank_level
let CARDS = []
let numberOfCard = 0
let betValue = 10

initCards = () => {
  const COLORS = ['C', 'D', 'H', 'S'];
  for(let i = 2; i <= 10; i++) {
    COLORS.forEach(col => {
      CARDS.push(i + col)
    });
  }
  COLORS.forEach(col => {
    CARDS.push('A' + col)
    CARDS.push('J' + col)
    CARDS.push('Q' + col)
    CARDS.push('K' + col)
  });
}

startNewGame = async () => {
  await initCards()
  level = 0
  bank_level = 0
  numberOfCard = 0
  betValue = document.getElementById('betValue').value

  if(betValue <= 0) {
    alert("Vous devez miser quelque chose")
    return
  }
  else if (Number.isInteger(betValue)) {
    alert("La mise doit être un entier.")
    return
  }
  else if(money < betValue) {
    alert("Vous n'avez plus d'argent pour parier")
    return
  }
  else if(bank_money < betValue) {
    alert("Vous avez déjà dévalisé la banque")
    return
  }

  document.getElementById('startGameOptions').style.display = 'none';
  document.getElementById('inGame').style.display = 'block';
  document.getElementById('bankHand').innerHTML = "";
  document.getElementById('yourHand').innerHTML = "";
  
  await addCardOnBankHand()
  await addCardOnYourHand()
  await addCardOnYourHand()
  
  updateMoney(betValue)
}

updateMoney = (value, playerBet = true) => {
  value = parseInt(value)
  if(playerBet) {
    money -= value
    bank_money += value
  } else {
    money += value
    bank_money -= value
  }
  document.getElementById('money-bank').textContent = bank_money;
  document.getElementById('money').textContent = money;
}

getRandomCard = () => {
  const random = Math.floor(Math.random()*CARDS.length);
  const card = CARDS[random];
  CARDS.splice(random, 1);
  return card;
}

addCardOnBankHand = () => {
  const card = getRandomCard()
  const value = card.length === 3 ? card.substring(0, 2) : card.substring(0, 1);
  if(value === 'K' || value === 'Q' || value === 'J') {
    bank_level += 10;
  } else if (value === 'A') {
    if(bank_level+11 <= 21) {
      bank_level += 11
    } else {
      bank_level += 1
    }
  } else {
    bank_level += parseInt(value);
  }
  const img = document.createElement("img");
  img.src = "./cards/" + card + ".png";
  document.getElementById('bankHand').appendChild(img);
  document.getElementById('bank-level').textContent = bank_level;
}

addCardOnYourHand = () => {
  const card = getRandomCard()
  const value = card.length === 3 ? card.substring(0, 2) : card.substring(0, 1);
  if(value === 'K' || value === 'Q' || value === 'J') {
    level += 10;
  } else if (value === 'A') {
    if(level+11 <= 21) {
      level += 11
    } else {
    level += 1
    }
  } else {
    level += parseInt(value);
  }
  const img = document.createElement("img");
  img.src = "./cards/" + card + ".png";
  img.alt = card;
  document.getElementById('yourHand').appendChild(img);
  document.getElementById('level').textContent = level;
  numberOfCard++;
}

clickNewCard = async () => {
  await addCardOnYourHand();
  if (checkIsGameOver()) {
    setTimeout(function(){
      endGame();
    }, 100);
  }
}

endGame = () => {
  let win = false;
  if(level === 21 || (level < 21 && bank_level > 21) || (level < 21 && bank_level < 21 && level > bank_level)) {
    win = true
  }
  if(win) {
    displayWin()
    updateMoney(betValue*2, false)
  }
  else {
    displayLoose()
  }
  document.getElementById('startGameOptions').style.display = 'block';
  document.getElementById('inGame').style.display = 'none';
}

displayWin = () => {
  alert("gagné")
}

displayLoose = () => {
  alert("perdu")
}

clickStandButton = () => {
  let countIteration = 0
  while (bank_level < 21 && countIteration < 4 && bank_level <= level ) {
    addCardOnBankHand();
    countIteration++;
  }
  setTimeout(function(){
    endGame();
  }, 100);
}

checkIsGameOver = () => {
  return level >= 21 || numberOfCard === 5;
}

let gameButton = document.getElementById('newGameButton')
let cardButton = document.getElementById('newCardButton')
let standButton = document.getElementById('standButton')
gameButton.addEventListener('click', startNewGame);
cardButton.addEventListener('click', clickNewCard);
standButton.addEventListener('click', clickStandButton);
document.getElementById('money').textContent = money;
document.getElementById('money-bank').textContent = bank_money;
