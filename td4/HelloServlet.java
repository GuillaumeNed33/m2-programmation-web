package fr.ub.m2.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.Enumeration;

@WebServlet(urlPatterns="/world")
public class HelloServlet extends HttpServlet {
    
    private void printHeaders(HttpServletRequest req, PrintWriter out) {
        out.println("<b>Request Method: </b>" + req.getMethod() + "<br/>");
        out.println("<b>Request URI: </b>" + req.getRequestURI() + "<br/>");
        out.println("<b>Request Protocol: </b>" + req.getProtocol() + "<br/><br/>");
        out.println("<table><thead>\n");
        out.println("\t<tr>\n<th>Header Name</th><th>Header Value</th></tr>\n");
        out.println("</thead><tbody>");
        Enumeration headerNames = req.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            out.println("\t<tr>\n\t\t<td>" + headerName + "</td>");
            out.println("<td>" + req.getHeader(headerName) + "</td>\n");
            out.println("\t</tr>\n");
        }
        out.println("</tbody></table>");
    }
    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String parameterValue = req.getParameter("name");
        if(parameterValue == null || parameterValue == "") {
            if(req.getSession().getAttribute("name") != null && req.getSession().getAttribute("name") != "") {
                parameterValue = (String) req.getSession().getAttribute("name");
            } else {
                parameterValue = "world";
            }
        }
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter(); 
        out.println("<html>");
        out.println("<body>");
        out.println("[GET] Hello " + parameterValue + "!<br/>");
        //printHeaders(req, out);
        out.println("</body>");
        out.println("</html>");
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = req.getReader().readLine();
        if(content == null || content == "") {
            String userName = (String) req.getSession().getAttribute("name");
            content = userName;
        } else {
            req.getSession().setAttribute("name", content);
        }
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter(); 
        out.println("[POST] Hello " + content + "! <br/>");
        printHeaders(req, out);
    }
    
}

