package fr.ub.m2.servlet;

import java.io.*;
import java.util.HashMap;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(urlPatterns="/login")
public class LoginServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        RequestDispatcher view = req.getRequestDispatcher("./login.html");
        view.forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        
        ServletContext servletContext = getServletContext();
        HashMap<String,String> profiles = (HashMap) servletContext.getAttribute( "profiles" );
        String passwordToVerify = (String) profiles.get(login);
        
        if(password.equals(passwordToVerify)) {
            req.getSession().setAttribute("name", login);
            resp.sendRedirect("/hello/home");  
        } else {
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter(); 
            out.println("<html>");
            out.println("<body>");
            out.println("Invalid login<br/>");
            out.println("<a href='/hello/login'>Back to login page</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
}

