package fr.ub.m2.servlet;

import java.io.*;
import java.util.HashMap;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(urlPatterns="/signup")
public class SignupServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        RequestDispatcher view = req.getRequestDispatcher("./signup.html");
        view.forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        
        if(login == null || login == "" || password == null || password == "") {
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter(); 
            out.println("<html>");
            out.println("<body>");
            out.println("Invalid inputs <br/>");
            out.println("<a href='/hello/login'>Back to signup page</a>");
            out.println("</body>");
            out.println("</html>");
        } else {
            //save info to context
            ServletContext servletContext = getServletContext();
            HashMap<String,String> profiles = (HashMap) servletContext.getAttribute( "profiles" );
            if(profiles == null) {
                profiles = new HashMap<>();
            }
            profiles.put(login, password);
            getServletContext().setAttribute( "profiles", profiles.clone() );

            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter(); 
            out.println("<html>");
            out.println("<body>");
            out.println("Account created successfuly<br/>");
            out.println("<a href='/hello/login'>Go to login page</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
}

