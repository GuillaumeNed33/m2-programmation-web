package fr.ub.m2.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(urlPatterns="/redirect")
public class RedirectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        RequestDispatcher view = req.getRequestDispatcher("./redirect.html");
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = req.getReader().readLine();
        if(content == null || content == "") {
            req.getRequestDispatcher("/redirect").forward(req, resp);
        } else {
            resp.sendRedirect("http://www.google.com/search?q=" + content.toString());  
        }
    }

}

