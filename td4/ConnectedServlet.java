package fr.ub.m2.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(urlPatterns="/home")
public class ConnectedServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getSession().getAttribute("name") == null || req.getSession().getAttribute("name") == "") {
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter(); 
            out.println("<html>");
            out.println("<body>");
            out.println("You're not connected<br/>");
            out.println("<a href='/hello/login'>Back to login page</a>");
            out.println("</body>");
            out.println("</html>");
        } else {
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter(); 
            out.println("<html>");
            out.println("<body>");
            out.println("Welcome ! You're connected !");
            out.println("</body>");
            out.println("</html>");
        }
    }    
}

